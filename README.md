# kibako.gitlab.io

GitLab Page for the main project `kibako`.

This repo only exists due to GitLabs weird naming rules for group-level pages.

## Build

The only thing this repo does, is to provide a pipeline that fetches the latest release artifact and host it as a page.
All of the code is located in https://gitlab.com/kibako/kibako
